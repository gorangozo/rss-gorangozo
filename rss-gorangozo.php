<?php

/**
 * The plugin bootstrap file
 *
 * @link              https://gorangozo.com/
 * @since             1.0.0
 * @package           Rss_Gorangozo
 *
 * @wordpress-plugin
 * Plugin Name:       RSS Goran Gozo
 * Plugin URI:        https://gorangozo.com/
 * Description:       RSS widget to display an RSS feed with vertical carousel.
 * Version:           1.0.0
 * Author:            Goran Gozo
 * Author URI:        https://gorangozo.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       rss-gorangozo
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Current plugin version.
 */
define( 'RSS_GORANGOZO_VERSION', '1.0.0' );

/**
 * The core plugin class.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-rss-gorangozo.php';

/**
 * Begins execution of the plugin.
 *
 * @since    1.0.0
 */
function run_rss_gorangozo() {

	$plugin = new Rss_Gorangozo();
	$plugin->run();

}

run_rss_gorangozo();
