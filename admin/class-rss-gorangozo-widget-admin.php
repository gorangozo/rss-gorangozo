<?php

/**
 * The admin functionality of the widget.
 *
 * @since      1.0.0
 * @package    Rss_Gorangozo
 * @subpackage Rss_Gorangozo/admin
 * @author     Goran Gozo <goran.gozo@gmail.com>
 */
class Rss_Gorangozo_Widget_Admin {

	/**
	 * Display widget form in admin area
	 *
	 * @param array $instance      The current widget instance's settings.
	 * @param int   $number        Unique ID number of the current instance.
	 * @param array $widget_fields Id attributes and name attributes for use in WP_Widget::form() fields
	 */
	public function display_widget_form( $instance, $number, $widget_fields ): void {
		if ( empty( $instance ) ) {
			$instance = array(
				'title'        => '',
				'url'          => '',
				'items'        => 5,
				'error'        => false,
				'show_summary' => 0,
				'show_author'  => 0,
				'show_date'    => 0
			);
		}
		$instance['number'] = $number;

		$this->widget_rss_form( $instance, null, $widget_fields );
	}

	/**
	 * Updates a particular instance of a widget.
	 *
	 * @param $new_instance New settings for this instance as input by the user via WP_Widget::form().
	 * @param $old_instance Old settings for this instance.
	 *
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function process_settings( $new_instance, $old_instance ): array {
		$testurl = ( isset( $new_instance['url'] ) && ( ! isset( $old_instance['url'] ) || ( $new_instance['url'] != $old_instance['url'] ) ) );

		return $this->widget_rss_process( $new_instance, $testurl );
	}

	/**
	 * Process RSS feed widget data and optionally retrieve feed items.
	 *
	 * The feed widget can not have more than 20 items or it will reset back to the
	 * default, which is 5.
	 *
	 * The resulting array has the feed title, feed url, feed link (from channel),
	 * feed items, error (if any), and whether to show summary, author, and date.
	 * All respectively in the order of the array elements.
	 *
	 * @since 1.0.0
	 *
	 * @param array $widget_rss RSS widget feed data. Expects unescaped data.
	 * @param bool  $check_feed Optional, default is true. Whether to check feed for errors.
	 *
	 * @return array
	 */
	private function widget_rss_process( $widget_rss, $check_feed = true ): array {
		$items = (int) $widget_rss['items'];
		if ( $items < 1 || 20 < $items ) {
			$items = 5;
		}
		$url          = esc_url_raw( strip_tags( $widget_rss['url'] ) );
		$title        = isset( $widget_rss['title'] ) ? trim( strip_tags( $widget_rss['title'] ) ) : '';
		$show_summary = isset( $widget_rss['show_summary'] ) ? (int) $widget_rss['show_summary'] : 0;
		$show_author  = isset( $widget_rss['show_author'] ) ? (int) $widget_rss['show_author'] : 0;
		$show_date    = isset( $widget_rss['show_date'] ) ? (int) $widget_rss['show_date'] : 0;

		if ( $check_feed ) {
			$rss   = fetch_feed( $url );
			$error = false;
			$link  = '';
			if ( is_wp_error( $rss ) ) {
				$error = $rss->get_error_message();
			} else {
				$link = esc_url( strip_tags( $rss->get_permalink() ) );
				while ( stristr( $link, 'http' ) != $link ) {
					$link = substr( $link, 1 );
				}

				$rss->__destruct();
				unset( $rss );
			}
		}

		return compact( 'title', 'url', 'link', 'items', 'error', 'show_summary', 'show_author', 'show_date' );
	}

	/**
	 * Display RSS widget options form.
	 *
	 * The options for what fields are displayed for the RSS form are all booleans
	 * and are as follows: 'url', 'title', 'items', 'show_summary', 'show_author',
	 * 'show_date'.
	 *
	 * @since 1.0.0
	 *
	 * @param array|string $args          Values for input fields.
	 * @param array        $inputs        Override default display options.
	 * @param array        $widget_fields Field names and id
	 */
	private function widget_rss_form( $args, $inputs = null, $widget_fields ): void {
		$default_inputs = array(
			'url'          => true,
			'title'        => true,
			'items'        => true,
			'show_summary' => true,
			'show_author'  => true,
			'show_date'    => true
		);
		$inputs         = wp_parse_args( $inputs, $default_inputs );

		$args['title'] = isset( $args['title'] ) ? $args['title'] : '';
		$args['url']   = isset( $args['url'] ) ? $args['url'] : '';
		$args['items'] = isset( $args['items'] ) ? (int) $args['items'] : 0;

		if ( $args['items'] < 1 || 20 < $args['items'] ) {
			$args['items'] = 5;
		}

		$args['show_summary'] = isset( $args['show_summary'] ) ? (int) $args['show_summary'] : (int) $inputs['show_summary'];
		$args['show_author']  = isset( $args['show_author'] ) ? (int) $args['show_author'] : (int) $inputs['show_author'];
		$args['show_date']    = isset( $args['show_date'] ) ? (int) $args['show_date'] : (int) $inputs['show_date'];

		if ( ! empty( $args['error'] ) ) {
			echo '<p class="widget-error"><strong>' . __( 'RSS Error:', 'rss-gorangozo' ) . '</strong> ' . $args['error'] . '</p>';
		}

		if ( $inputs['url'] ) :
			?>
            <p>
                <label for="<?php echo esc_attr( $widget_fields['url']['id'] ); ?>"><?php _e( 'Enter the RSS feed URL here:', 'rss-gorangozo' ); ?></label>
                <input class="widefat" id="<?php echo esc_attr( $widget_fields['url']['id'] ); ?>"
                       name="<?php echo esc_attr( $widget_fields['url']['name'] ); ?>" type="text"
                       value="<?php echo esc_url( $args['url'] ); ?>"/>
            </p>
		<?php endif;
		if ( $inputs['title'] ) : ?>
            <p>
                <label for="<?php echo esc_attr( $widget_fields['title']['id'] ); ?>"><?php _e( 'Give the feed a title (optional):', 'rss-gorangozo' ); ?></label>
                <input class="widefat" id="<?php echo esc_attr( $widget_fields['title']['id'] ); ?>"
                       name="<?php echo esc_attr( $widget_fields['title']['name'] ); ?>" type="text"
                       value="<?php echo esc_attr( $args['title'] ); ?>"/>
            </p>
		<?php endif;
		if ( $inputs['items'] ) : ?>
            <p>
                <label for="<?php echo esc_attr( $widget_fields['items']['id'] ); ?>"><?php _e( 'How many items would you like to display?', 'rss-gorangozo' ); ?></label>
                <select id="<?php echo esc_attr( $widget_fields['items']['id'] ); ?>"
                        name="<?php echo esc_attr( $widget_fields['items']['name'] ); ?>">
					<?php
					for ( $i = 1; $i <= 20; ++ $i ) {
						echo "<option value='$i' " . selected( $args['items'], $i, false ) . ">$i</option>";
					}
					?>
                </select>
            </p>
		<?php endif;
		if ( $inputs['show_summary'] ) : ?>
            <p>
                <input id="<?php echo esc_attr( $widget_fields['show_summary']['id'] ); ?>"
                      name="<?php echo esc_attr( $widget_fields['show_summary']['name'] ); ?>" type="checkbox"
                      value="1" <?php checked( $args['show_summary'] ); ?> />
                <label for="<?php echo esc_attr( $widget_fields['show_summary']['id'] ); ?>"><?php _e( 'Display item content?', 'rss-gorangozo' ); ?></label>
            </p>

		<?php endif;
		if ( $inputs['show_author'] ) : ?>
            <p>
                <input id="<?php echo esc_attr( $widget_fields['show_author']['id'] ); ?>"
                      name="<?php echo esc_attr( $widget_fields['show_author']['name'] ); ?>" type="checkbox"
                      value="1" <?php checked( $args['show_author'] ); ?> />
                <label for="<?php echo esc_attr( $widget_fields['show_author']['id'] ); ?>"><?php _e( 'Display item author if available?', 'rss-gorangozo' ); ?></label>
            </p>

		<?php endif;
		if ( $inputs['show_date'] ) : ?>
            <p>
                <input id="<?php echo esc_attr( $widget_fields['show_date']['id'] ); ?>"
                      name="<?php echo esc_attr( $widget_fields['show_date']['name'] ); ?>" type="checkbox"
                      value="1" <?php checked( $args['show_date'] ); ?>/>
                <label for="<?php echo esc_attr( $widget_fields['show_date']['id'] ); ?>"><?php _e( 'Display item date?', 'rss-gorangozo' ); ?></label>
            </p>
		<?php
		endif;
	}

}