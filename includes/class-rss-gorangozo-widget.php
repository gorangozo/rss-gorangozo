<?php

/**
 * widget
 *
 * @link       https://gorangozo.com/
 * @since      1.0.0
 *
 * @package    Rss_Gorangozo
 * @subpackage Rss_Gorangozo/includes
 */

/**
 * Widget.
 *
 * Widget.
 *
 * @since      1.0.0
 * @package    Rss_Gorangozo
 * @subpackage Rss_Gorangozo/includes
 * @author     Goran Gozo <goran.gozo@gmail.com>
 */
class Rss_Gorangozo_Widget extends WP_Widget {

	/**
	 * @var Rss_Gorangozo_Widget_Public $widget_public Public part of the widget.
	 */
	protected $widget_public;

	/**
	 * @var Rss_Gorangozo_Widget_Admin $widget_admin Admin part of the widget.
	 */
	protected $widget_admin;

	/**
	 * Sets up a new RSS widget instance.
	 *
	 * @since 1.0.0
	 *
	 * @param $widget_public    Rss_Gorangozo_Widget_Public   The public part of the widget.
	 * @param $widget_admin     Rss_Gorangozo_Widget_Admin    The admin part of the widget.
	 */
	public function __construct( $widget_public, $widget_admin ) {
		$this->widget_public = $widget_public;
		$this->widget_admin  = $widget_admin;

		$widget_ops  = array(
			'description'                 => __( 'Entries from any RSS or Atom feed.', 'rss-gorangozo' ),
			'customize_selective_refresh' => false,
		);
		$control_ops = array( 'width' => 400, 'height' => 200 );
		parent::__construct( 'rss_gorangozo', __( 'RSS Goran Gozo', 'rss-gorangozo' ), $widget_ops, $control_ops );
	}

	/**
	 * Outputs the content for the current RSS widget instance.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current RSS widget instance.
	 */
	public function widget( $args, $instance ): void {
		if ( isset( $instance['error'] ) && $instance['error'] ) {
			return;
		}

		$this->widget_public->display_widget( $args, $instance, $this->id_base );
	}

	/**
	 * Handles updating settings for the current RSS widget instance.
	 *
	 * @since 1.0.0
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 *
	 * @return array Updated settings to save.
	 */
	public function update( $new_instance, $old_instance ): array {
		return $this->widget_admin->process_settings( $new_instance, $old_instance );
	}

	/**
	 * Outputs the settings form for the RSS widget.
	 *
	 * @since 1.0.0
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ): void {

		$fields_list   = array( 'url', 'title', 'items', 'show_summary', 'show_author', 'show_date' );
		$widget_fields = array();

		foreach ( $fields_list as $field ) {
			$widget_fields[ $field ] = $this->get_field_data( $field );
		}

		$this->widget_admin->display_widget_form( $instance, $this->number, $widget_fields );
	}

	/**
	 * Constructs id attributes and name attributes for use in WP_Widget::form() fields.
	 *
	 * @param string $field_name    Field name
	 *
	 * @return array                ID attribute and name attribute for `$field_name`
	 */
	private function get_field_data( $field_name ): array {
		$id_attribute   = $this->get_field_id( $field_name );
		$name_attribute = $this->get_field_name( $field_name );

		return array( 'id' => $id_attribute, 'name' => $name_attribute );
	}

}
