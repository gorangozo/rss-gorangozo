<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://gorangozo.com/
 * @since      1.0.0
 *
 * @package    Rss_Gorangozo
 * @subpackage Rss_Gorangozo/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Rss_Gorangozo
 * @subpackage Rss_Gorangozo/includes
 * @author     Goran Gozo <goran.gozo@gmail.com>
 */
class Rss_Gorangozo {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Rss_Gorangozo_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'RSS_GORANGOZO_VERSION' ) ) {
			$this->version = RSS_GORANGOZO_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'rss-gorangozo';

		$this->load_dependencies();
		$this->define_widget_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Rss_Gorangozo_Loader. Orchestrates the hooks of the plugin.
	 * - Rss_Gorangozo_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies(): void {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rss-gorangozo-loader.php';

		/**
		 * The class responsible for the widget.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-rss-gorangozo-widget.php';

		/**
		 * The class responsible for the public part of the widget.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-rss-gorangozo-widget-public.php';

		/**
		 * The class responsible for the admin part of the widget.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-rss-gorangozo-widget-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-rss-gorangozo-public.php';

		$this->loader = new Rss_Gorangozo_Loader();

	}

	/**
	 * Register all of the hooks related to the widget.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_widget_hooks(): void {

		$this->loader->add_action( 'widgets_init', $this, 'register_widget' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks(): void {

		$plugin_public = new Rss_Gorangozo_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run(): void {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name(): string {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Rss_Gorangozo_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader(): Rss_Gorangozo_Loader {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version(): string {
		return $this->version;
	}

	/**
	 * Callback to register widget.
	 *
	 * @since    1.0.0
	 */
	public function register_widget(): void {
		$widget_public = new Rss_Gorangozo_Widget_Public();
		$widget_admin  = new Rss_Gorangozo_Widget_Admin();
		$rss_widget    = new Rss_Gorangozo_Widget( $widget_public, $widget_admin );
		register_widget( $rss_widget );
	}

}
