<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://gorangozo.com/
 * @since      1.0.0
 *
 * @package    Rss_Gorangozo
 * @subpackage Rss_Gorangozo/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two hooks to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Rss_Gorangozo
 * @subpackage Rss_Gorangozo/public
 * @author     Goran Gozo <goran.gozo@gmail.com>
 */
class Rss_Gorangozo_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 *
	 * @param      string $plugin_name The name of the plugin.
	 * @param      string $version     The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles(): void {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/rss-gorangozo-public.css', array(),
			$this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts(): void {

		wp_enqueue_script( 'jquery-easy-ticker-js',
			plugin_dir_url( __FILE__ ) . 'js/jquery.easy-ticker.min.js',
			array( 'jquery' ), '2.0.0', true );

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/rss-gorangozo-public.js',
			array( 'jquery', 'jquery-easy-ticker-js' ), $this->version, true );

	}

}
