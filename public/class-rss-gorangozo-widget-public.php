<?php

/**
 * The public-facing functionality of the widget.
 *
 * @since      1.0.0
 * @package    Rss_Gorangozo
 * @subpackage Rss_Gorangozo/public
 * @author     Goran Gozo <goran.gozo@gmail.com>
 */
class Rss_Gorangozo_Widget_Public {

	/**
	 * @var int     Number of RSS items to add to markup.
	 */
	const rss_items_available = 100;

	/**
	 * Display the widget on the front-facing part of the website.
	 *
	 * @param array  $args     Display arguments including 'before_title', 'after_title',
	 *                         'before_widget', and 'after_widget'.
	 * @param array  $instance Settings for the current RSS widget instance.
	 * @param string $id_base  ID base of widget.
	 */
	public function display_widget( $args, $instance, $id_base ): void {
		$url = $this->extract_url_from_settings( $instance );

		if ( ! $this->is_url_sane( $url ) ) {
			return;
		}

		$rss = fetch_feed( $url );

		list( $title_from_feed, $desc, $link ) = $this->get_channel_meta_from_rss( $rss );

		$title = ! empty( $instance['title'] ) ? $instance['title'] : $title_from_feed;

		if ( empty( $title ) ) {
			$title = ! empty( $desc ) ? $desc : __( 'Unknown Feed', 'rss-gorangozo' );
		}

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $id_base );

		echo $args['before_widget'];
		$this->print_widget_header( $args, $url, $title, $link );

		$this->widget_rss_output( $rss, $instance );
		echo $args['after_widget'];

		if ( ! is_wp_error( $rss ) ) {
			$rss->__destruct();
		}
		unset( $rss );
	}

	/**
	 * Extract URL to feed to fetch.
	 *
	 * @param array $instance Settings for the current RSS widget instance.
	 *
	 * @return string $url URL to feed to fetch.
	 */
	private function extract_url_from_settings( $instance ): string {
		$url = ! empty( $instance['url'] ) ? $instance['url'] : '';
		while ( stristr( $url, 'http' ) != $url ) {
			$url = substr( $url, 1 );
		}

		return $url;
	}

	/**
	 * Check if the URL looks good.
	 *
	 * @param string $url URL to feed to fetch.
	 *
	 * @return bool
	 */
	private function is_url_sane( $url ): bool {

		if ( empty( $url ) ) {
			return false;
		}

		// self-url destruction sequence
		if ( in_array( untrailingslashit( $url ), array( site_url(), home_url() ) ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Gets meta data from the RSS feed
	 *
	 * @param $rss WP_Error|SimplePie RSS feed
	 *
	 * @return array An array with meta data
	 */
	private function get_channel_meta_from_rss( $rss ): array {

		$title = '';
		$desc  = '';
		$link  = '';

		if ( ! is_wp_error( $rss ) ) {
			$desc = esc_attr( strip_tags( @html_entity_decode( $rss->get_description(), ENT_QUOTES,
				get_option( 'blog_charset' ) ) ) );

			$title = strip_tags( $rss->get_title() );

			$link = strip_tags( $rss->get_permalink() );
			while ( stristr( $link, 'http' ) != $link ) {
				$link = substr( $link, 1 );
			}
		}

		return array( $title, $desc, $link );
	}

	/**
	 * Prints out the widget header
	 *
	 * @param array  $args        Display arguments including 'before_title', 'after_title',
	 *                            'before_widget', and 'after_widget'.
	 * @param string $url         URL to RSS feed
	 * @param string $title       Title of widget instance
	 * @param string $link        URL to website
	 */
	private function print_widget_header( $args, $url, $title, $link ): void {
		$url  = strip_tags( $url );
		$icon = plugin_dir_url( __FILE__ ) . 'images/rss.png';

		if ( $title ) {
			echo $args['before_title'];

			echo '<a class="rsswidget" href="' . esc_url( $url ) . '">';
			echo '<img class="rss-widget-icon" style="border:0" width="14" height="14" src="' . esc_url( $icon ) . '" alt="RSS" />';
			echo '</a> ';
			echo '<a class="rsswidget" href="' . esc_url( $link ) . '">' . esc_html( $title ) . '</a>';

			echo '<a href="#" class="js-up arrow-up">';
			echo '<img class="arrow-up-img" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGhlaWdodD0iNTEycHgiIGlkPSJMYXllcl8xIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgdmVyc2lvbj0iMS4xIiB2aWV3Qm94PSIwIDAgNTEyIDUxMiIgd2lkdGg9IjUxMnB4IiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cG9seWdvbiBwb2ludHM9IjM5Ni42LDE2MCA0MTYsMTgwLjcgMjU2LDM1MiA5NiwxODAuNyAxMTUuMywxNjAgMjU2LDMxMC41ICIvPjwvc3ZnPg==">';
			echo '</a>';
			echo '<a href="#" class="js-down arrow-down">';
			echo '<img class="arrow-up-img" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGhlaWdodD0iNTEycHgiIGlkPSJMYXllcl8xIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgdmVyc2lvbj0iMS4xIiB2aWV3Qm94PSIwIDAgNTEyIDUxMiIgd2lkdGg9IjUxMnB4IiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cG9seWdvbiBwb2ludHM9IjM5Ni42LDM1MiA0MTYsMzMxLjMgMjU2LDE2MCA5NiwzMzEuMyAxMTUuMywzNTIgMjU2LDIwMS41ICIvPjwvc3ZnPg==">';
			echo '</a>';

			echo $args['after_title'];
		}
	}

	/**
	 * Display the RSS entries in a list.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Error|SimplePie $rss  RSS feed
	 * @param array              $args Widget arguments
	 */
	private function widget_rss_output( $rss, $args = array() ): void {
		if ( ! is_object( $rss ) && ( ! is_wp_error( $rss ) | ! $rss instanceof SimplePie ) ) {
			return;
		}

		if ( is_wp_error( $rss ) ) {
			if ( current_user_can( 'manage_options' ) ) {
				echo '<p><strong>' . __( 'RSS Error:', 'rss-gorangozo' ) . '</strong> ' . $rss->get_error_message() . '</p>';
			}

			return;
		}

		if ( ! $rss->get_item_quantity() ) {
			echo '<ul><li>' . __( 'An error has occurred, which probably means the feed is down. Try again later.', 'rss-gorangozo' ) . '</li></ul>';
			$rss->__destruct();
			unset( $rss );

			return;
		}

		$default_args = array( 'show_author' => 0, 'show_date' => 0, 'show_summary' => 0, 'items' => 0 );
		$args         = wp_parse_args( $args, $default_args );

		$items = (int) $args['items'];
		if ( $items < 1 || 20 < $items ) {
			$items = 5;
		}
		$show_summary = (int) $args['show_summary'];
		$show_author  = (int) $args['show_author'];
		$show_date    = (int) $args['show_date'];

		echo '<div class="js-rss-gorangozo-app rss-gorangozo-wrapper" data-visible="' . $items . '"><ul>';

		foreach ( $rss->get_items( 0, self::rss_items_available ) as $item ) {
			$this->print_single_feed_item( $item, $show_summary, $show_date, $show_author );
		}

		echo '</ul></div>';

		$rss->__destruct();
		unset( $rss );
	}

	/**
	 * Prints out a single item from the RSS feed
	 *
	 * @param SimplePie_Item $item         A single item from the RSS feed
	 * @param bool           $show_summary Whether to show summary for the item
	 * @param bool           $show_date    Whether to show date for the item
	 * @param bool           $show_author  Whether to show author for the item
	 */
	private function print_single_feed_item( $item, $show_summary, $show_date, $show_author ): void {
		$link = $item->get_link();
		while ( stristr( $link, 'http' ) != $link ) {
			$link = substr( $link, 1 );
		}
		$link = esc_url( strip_tags( $link ) );

		$title = esc_html( trim( strip_tags( $item->get_title() ) ) );
		if ( empty( $title ) ) {
			$title = __( 'Untitled', 'rss-gorangozo' );
		}

		$desc = @html_entity_decode( $item->get_description(), ENT_QUOTES, get_option( 'blog_charset' ) );
		$desc = esc_attr( wp_trim_words( $desc, 55, ' [&hellip;]' ) );

		$summary = '';
		if ( $show_summary ) {
			$summary = $desc;

			// Change existing [...] to [&hellip;].
			if ( '[...]' == substr( $summary, - 5 ) ) {
				$summary = substr( $summary, 0, - 5 ) . '[&hellip;]';
			}

			$summary = '<div class="rssSummary">' . esc_html( $summary ) . '</div>';
		}

		$date = '';
		if ( $show_date ) {
			$date = $item->get_date( 'U' );

			if ( $date ) {
				$date = ' <span class="rss-date">' . date_i18n( get_option( 'date_format' ), $date ) . '</span>';
			}
		}

		$author = '';
		if ( $show_author ) {
			$author = $item->get_author();
			if ( is_object( $author ) ) {
				$author = $author->get_name();
				$author = ' <cite>' . esc_html( strip_tags( $author ) ) . '</cite>';
			}
		}

		if ( $link == '' ) {
			echo "<li>$title{$date}{$summary}{$author}</li>";
		} elseif ( $show_summary ) {
			echo "<li><a class='rsswidget' href='$link'>$title</a>{$date}{$summary}{$author}</li>";
		} else {
			echo "<li><a class='rsswidget' href='$link'>$title</a>{$date}{$author}</li>";
		}
	}

}