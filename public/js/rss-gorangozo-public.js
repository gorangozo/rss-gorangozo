(function ($) {
    'use strict';

    $(document).ready(function () {

        $('.js-rss-gorangozo-app').each(function () {
            var visible = $(this).attr('data-visible');
            var interval = 4000;
            var ticker = $(this).easyTicker({
                'visible': visible,
                'interval': interval,
                controls: {
                    'up': '.js-up',
                    'down': '.js-down'
                }
            });
        });

    });

})(jQuery);
