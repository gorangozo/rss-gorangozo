=== RSS Goran Gozo ===
Contributors: gorangozo
Donate link: https://gorangozo.com/
Tags: rss
Requires at least: 4.0.0
Tested up to: 5.0.2
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

RSS widget to display an RSS feed with vertical carousel.

== Description ==

RSS widget to display an RSS feed with vertical carousel.
